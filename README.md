This repository contains supporting material (meshes, problem definitions) for the associated paper:

Vijay S. Mahadevan, Elia Merzari, Timothy Tautges, Rajeev Jain, Aleksandr Obabko, Michael Smith, Paul Fischer, "High resolution coupled physics solvers for analyzing fine-scale nuclear reactor design problems", Royal Society Int. Proc A, May 2014.

In the current repository:

1) The mesh files for both the SAHEX and XX09 single assembly problems with intermediate refinements is available.
2) A video of the actual simulated transient for the XX09 loss-of-heat-sink transient has been added for verification.
3) A description of the physics and the nonlinear feedback effects seen in the video is provided separately.

